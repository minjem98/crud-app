import React from "react";
import "./Button.css";

function Button({ text, handleClick, type = "button" }) {
  return (
    <button type={type} onClick={handleClick} id="ajouter-enregistrer-btn">
      {text}
    </button>
  );
}

export default Button;
