import React, { useState } from "react";
import "react-responsive-modal/styles.css";
import { Modal } from "react-responsive-modal";
import Button from "../Button/Button";
import Input from "../Input/Input";
import "./Modal.css";

function Modall({ showModal, closeModal, addUser, areInputsValid }) {
  const [userData, setUserData] = useState({
    id: Math.floor(Math.random() * 5999999 + 1).toString(),
    createdDate: undefined,
    status: "",
    firstName: "",
    lastName: "",
    userName: "",
    registrationNumber: "",
  });

  const handleChange = (e) => {
    setUserData({
      ...userData,
      [e.target.name]: e.target.value,
    });
  };

  const clearUserData = () => {
    setUserData({
      id: Math.floor(Math.random() * 5999999 + 1).toString(),
      createdDate: undefined,
      status: "",
      firstName: "",
      lastName: "",
      userName: "",
      registrationNumber: undefined,
    });
  };

  const handleSubmit = (e) => {
    addUser(e, userData);
    if (areInputsValid(userData)) {
      clearUserData();
      closeModal();
    }
  };
  return (
    <>
      <Modal open={showModal} onClose={closeModal} center={true}>
        <h2 className="pl-3">
          <b>Ajout d'utilisateurs</b>
        </h2>
        <br />

        <form onSubmit={handleSubmit} className="container add-user-form">
          <div className="row">
            <div className="col-sm-4">
              <Input
                type="text"
                label="Nom"
                name="lastName"
                value={userData.lastName}
                handleChange={handleChange}
              />
            </div>
            <div className="col-sm-4">
              <Input
                type="text"
                label="Prénom"
                name="firstName"
                value={userData.firstName}
                handleChange={handleChange}
              />
            </div>
            <div className="col-sm-4">
              <Input
                type="select"
                label="Etat"
                name="status"
                value={userData.status}
                handleChange={handleChange}
              />
            </div>
          </div>
          <div className="row">
            <div className="col-sm-4">
              <Input
                type="text"
                label="Nom d'utilisateur"
                name="userName"
                value={userData.userName}
                handleChange={handleChange}
              />
            </div>
            <div className="col-sm-4">
              <Input
                type="date"
                label="Date de création"
                name="createdDate"
                value={userData.createdDate}
                handleChange={handleChange}
              />
            </div>
          </div>

          <div className="row">
            <div className="col-sm-4">
              <Input
                type="number"
                label="Matricule"
                name="registrationNumber"
                value={userData.registrationNumber}
                handleChange={handleChange}
              />
            </div>
          </div>
        </form>
        <div className="ajouter-btn-container ">
          <Button type="submit" text="Enregistrer" handleClick={handleSubmit} />
        </div>
      </Modal>
    </>
  );
}

export default Modall;
