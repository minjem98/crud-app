import React from "react";
import "./Input.css";
function Input({ label, name, type, value, handleChange }) {
  return (
    <>
      <label className="input-label">
        <span className="input-label-text">{label} </span>
        <input
          required
          name={name}
          type={type}
          value={value}
          onChange={handleChange}
          className="add-user-input"
        />
      </label>
    </>
  );
}

export default Input;
