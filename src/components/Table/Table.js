import React from "react";
import "./Table.css";

function Table({ headerData, bodyData, deleteUser }) {
  const colorOfStatus = (status) => {
    switch (status) {
      case "En validation":
        return "#FDB64D";
      case "Validé":
        return "#5BE881";
      case "Rejeté":
        return "#FF0000";
      default:
        return status;
    }
  };
  if (!bodyData.length)
    return (
      <h3 className="text-center mb-5 mt-5">
        La liste des utilisateurs est encore vide, veuillez ajouter un
        utlisateur
      </h3>
    );
  return (
    <>
      <div className="table-responsive-md">
        <table className="table shadow p-2 mb-4 bg-white rounded">
          <thead>
            <tr>
              {headerData.map((header) => (
                <th
                  scope="col"
                  key={Math.random() * 1000 + 1}
                  className="table-head"
                >
                  {header}
                </th>
              ))}
            </tr>
          </thead>
          <tbody>
            {bodyData.map(
              ({
                id,
                createdDate,
                status,
                firstName,
                lastName,
                userName,
                registrationNumber,
              }) => (
                <tr key={Math.random() * 1000 + 1}>
                  <th scope="row">{id}</th>
                  <td>{createdDate.split("T")[0]}</td>
                  <td>
                    <span
                      style={{
                        backgroundColor: colorOfStatus(status),
                        display: "inline-block",
                        color: "#fff",
                        textAlign: "center",
                        borderRadius: 10,
                        padding: 9,
                        fontSize: 13,
                        width: "80%",
                      }}
                    >
                      {" "}
                      {status}
                    </span>
                  </td>
                  <td>{firstName}</td>
                  <td>{lastName}</td>
                  <td>{userName}</td>
                  <td>{registrationNumber}</td>
                  <td id={id}>
                    <i
                      class="fa fa-trash-o"
                      aria-hidden="true"
                      onClick={(e) => {
                        console.log(e.target.parentNode.id);
                        deleteUser(e.target.parentNode.id);
                      }}
                    ></i>
                  </td>
                </tr>
              )
            )}
          </tbody>
        </table>
      </div>
    </>
  );
}

export default Table;
