import React, { useState, useEffect } from "react";
import Button from "../../components/Button/Button";
import Modal from "../../components/Modal/Modal";
import Table from "../../components/Table/Table";
import "./home.css";

let users = [
  {
    id: "123456789",
    createdDate: "2021-01-06T00:00:00.000Z",
    status: "En validation",
    firstName: "Mohamed",
    lastName: "Taha",
    userName: "mtaha",
    registrationNumber: "2584",
  },
  {
    id: "987654321",
    createdDate: "2021-07-25T00:00:00.000Z",
    status: "Validé",
    firstName: "Hamid",
    lastName: "Orrich",
    userName: "horrich",
    registrationNumber: "1594",
  },
  {
    id: "852963741",
    createdDate: "2021-09-15T00:00:00.000Z",
    status: "Rejeté",
    firstName: "Rachid",
    lastName: "Mahidi",
    userName: "rmahidi",
    registrationNumber: "3576",
  },
];
const headerData = [
  "ID",
  "Date de création",
  "Etat",
  "Nom",
  "Prénom",
  "Nom d'utilisateur",
  "Matricule",
  "Action",
];
function Home() {
  const [listOfUsers, setList] = useState(users);
  const [showModal, setShowModal] = useState(false);

  const openModal = () => setShowModal(true);
  const closeModal = () => setShowModal(false);

  const areInputsValid = (userData) => {
    return Object.values(userData).every((value) => value !== undefined);
  };
  const addUser = (e, userData) => {
    e.preventDefault();
    areInputsValid(userData)
      ? setList([...listOfUsers, userData])
      : alert("Veuillez remplir tous les champs");
  };

  const deleteUser = (id) => {
    let newList = [...listOfUsers].filter((user) => user.id !== id);
    setList(newList);
  };

  useEffect(() => {}, [listOfUsers]);

  return (
    <>
      <div className="home-parent">
        <Table
          headerData={headerData}
          bodyData={listOfUsers}
          deleteUser={deleteUser}
        />
        <div className="container ajouter-btn-container shadow p-3 mb-4 bg-white rounded">
          <Button text="Ajouter utilisateur" handleClick={openModal} />
        </div>
        <Modal
          showModal={showModal}
          openModal={openModal}
          closeModal={closeModal}
          addUser={addUser}
          areInputsValid={areInputsValid}
        />
      </div>
    </>
  );
}

export default Home;
