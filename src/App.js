import { useState } from "react";
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";
import Home from "./pages/Home";

function App() {
  return (
    <div className="App container mt-5">
      <Home />
    </div>
  );
}

export default App;
